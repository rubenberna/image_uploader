import api from '../../api/imgur';
import { router } from '../../main';

const state = {
    images: []
};

const getters = {
    allImages: state => state.images
};

const actions = {
    async fetchImages({ rootState, commit }) {
        //rootState is a reference to all of the state that is stored inside our application. It gives us the hability to reach other modules. This is done in the index.js file. The Store has the module auth, from which we can now retreive data
       const { token } = rootState.auth; // this ES2015 sintax means we don't have to right token twice
       const response = await api.fetchImages(token);
       commit('setImages', response.data.data);
    },
    async uploadImages({ rootState, commit }, images) {
        // Get access token
        const { token } = rootState.auth;
        // Call our API module to do the upload
        await api.uploadImages(images, token);
        // Redirect our user to ImageList component
        router.push('/');
    }
};

const mutations = {
    setImages: (state, images) => {
        state.images = images;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};