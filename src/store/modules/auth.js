import api from '../../api/imgur';
import qs from 'qs'; //library used to manipulate, parse and generate querystrings
import { router } from '../../main'; // allows our user to navigate around our page. Inthis case, we import using {} because we're a importing a named export (line 10 in main.js)

const state = {
  token: window.localStorage.getItem('imgur_token')
  // rather than setting the token as null, which meant that the user would have to login everytime he comes to our page, we're using the browser localStorage to keep the token. We need to add this step to the finalizeLogin action as well
};

const getters = {
  isLoggedIn: (state) => {
// this state is not the same as the one above. it gets the computed value (which changes if the user is or not logged in)
    return !!state.token 
// !! flips the values into a simple 'false' or 'true'
// ES6 expression is state => !!state.token
  }
};


const actions = {
  login: () => {
      api.login();
// calls the function imported through the api file
  },
  finalizeLogin({commit}, hash) {
// hash is a placeholder for the url string passed by the authHandler method. The first 'commit' object is to call the mutation
    const query = qs.parse(hash.replace('#', ''))
    // we're replacing the # by '' because we don't need the hash character
    // the qs.parse will return an object with key-values. We want the access_token
    commit('setToken', query.access_token);
    window.localStorage.setItem('imgur_token', query.access_token);
    // this persists the token in the localStorage of the browser, so that it remembers the user. setItem takes a key name (random) and the piece of info we want to store in localStorage
    router.push('/');
    // it sends our user to this route/url after login
  },
  logout: ({ commit }) => {
      commit('setToken', null);
      window.localStorage.removeItem('imgur_token');
// the first argument is commit to call the mutation. The second argument is the token value.
  }
};

const mutations = {
  setToken: (state, token) =>{
// it always takes state as the first argument. The second argument (there can be countless) is the token itself
    state.token = token;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}
