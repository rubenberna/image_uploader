import Vuex from 'vuex';
import Vue from 'vue';
import auth from './modules/auth';
import images from './modules/images';

Vue.use(Vuex); // to make sure these two libraries are connected to each other. It's kind of like a handshake between the two libraries. In main.js, inside the vue instance, they agree on the follow-up: store holds all the data.


export default new Vuex.Store({
    modules: {
        auth,
        images
    }
});

// Store is the overall collection of our modules and the state, getters and methods inside of it
// the modules object stores all modules from our app