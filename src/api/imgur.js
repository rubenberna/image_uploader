import qs from 'qs';
// library that takes our params object to form up a url
import axios from 'axios';

const CLIENT_ID = '8dd4aad1a8db5ef';
const ROOT_URL = 'https://api.imgur.com';

export default {
    login() {
// form up a params object
        const querystring = {
            client_id: CLIENT_ID,
            response_type: 'token'
        };
        window.location = `${ROOT_URL}/oauth2/authorize?${qs.stringify(querystring)}`;
        /* this url is taken out the imgur api documentation:
        https://api.imgur.com/oauth2/authorize?client_id=YOUR_CLIENT_ID&response_type=REQUESTED_RESPONSE_TYPE&state=APPLICATION_STATE
        The window.location causes the user's browser to programatically navigate over to this url automatically
        */
    },
    fetchImages(token) {
        return axios.get(`${ROOT_URL}/3/account/me/images`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        /* url taken from https://apidocs.imgur.com/#ee366f7c-69e6-46fd-bf26-e93303f64c84
        one of the specifications in this get request is to add a header with an authorization and the bearer and access token as string.
        The access token is passed as an argument from the images.js action
        */
    },
    uploadImages(images, token) {
        // images is an "array like" object. This method turns it into an array to be able to iterate and upload one-by-one, because the imgur api requires to upload one image at a time. We later saved this into a const 'promises' to take the list of promises with all files together 
        const promises = Array.from(images).map(image => {
            const formData = new FormData();
            formData.append('image', image);
        /*   form data object is part of vanila js and allows us to use a reference to the file to pass the file itself. The append will add the image to the formData object.
         The imgur api docs requests us to pass a key. The first 'image' is the key, while image is the file 
         */
            return axios.post(`${ROOT_URL}/3/image`, formData, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
        });
        return Promise.all(promises);
        // Promise.all is a built-in function that takes an array of promises and waits for everyone of those individual promises to be resolved, and once that happens, Promise.all will resolve as well. This code will just wait for every bit of code to be completed before we allow this function to continue
    }
};