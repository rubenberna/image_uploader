import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import store from './store';
import AuthHandler from './components/AuthHandler';
import ImageList from './components/ImageList';
import UploadForm from './components/UploadForm';


Vue.use(VueRouter);

export const router = new VueRouter({
  mode: 'history',
  // defines the browser router option, that looks for the component after the domain name and not after the # character (notes on the diagram 8)
  routes: [
    { path: '/', component: ImageList },
    { path: '/upload/', component: UploadForm },
    { path: '/oauth2/callback', component: AuthHandler }
  ]
});

new Vue({
  store, // same as store: store. it passes the store to this vue instance. It provides data to the relation between vue and vuex
  router,
  el: '#app',
  render: h => h(App)
})
